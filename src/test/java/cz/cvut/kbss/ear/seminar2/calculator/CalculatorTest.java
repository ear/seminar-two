package cz.cvut.kbss.ear.seminar2.calculator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestServiceConfig.class})
public class CalculatorTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private Calculator calculator;

    @Test
    public void setupVerification() {
        assertNotNull(calculator);
    }
}
