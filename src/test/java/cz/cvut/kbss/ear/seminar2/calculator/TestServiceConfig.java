package cz.cvut.kbss.ear.seminar2.calculator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestServiceConfig {

    @Bean
    public Calculator calculator() {
        return new Calculator();
    }
}
