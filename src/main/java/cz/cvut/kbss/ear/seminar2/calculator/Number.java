package cz.cvut.kbss.ear.seminar2.calculator;

class Number implements Token {

    private final int value;

    Number(int value) {
        this.value = value;
    }

    @Override
    public int evaluate() {
        return value;
    }
}
