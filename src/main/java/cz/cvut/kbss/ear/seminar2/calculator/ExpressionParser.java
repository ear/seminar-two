package cz.cvut.kbss.ear.seminar2.calculator;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ExpressionParser {

    /**
     * Parser the given string and returns an abstract representation of the
     * mathematical expression it represents.
     *
     * @param text The expression as a string
     * @return Evaluable expression
     */
    public Expression parse(String text) {
        List<String> tokens;
        try {
            tokens = tokenize(text);
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to parse expression " + text, e);
        }
        return generateExpression(tokens);
    }

    private List<String> tokenize(String expression) throws IOException {
        final StreamTokenizer tokenizer = new StreamTokenizer(new StringReader(expression));
        final List<String> tokens = new ArrayList<>();
        while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
            switch (tokenizer.ttype) {
                case StreamTokenizer.TT_NUMBER:
                    tokens.add(String.valueOf(tokenizer.nval));
                default:  // operator
                    tokens.add(String.valueOf((char) tokenizer.ttype));
            }
        }
        return tokens;
    }

    private Expression generateExpression(List<String> tokens) throws NumberFormatException {
        Stack<Token> stack = new Stack<>();
        for (String token : tokens) {
            if (isNumber(token)) {
                int num = (int) Double.parseDouble(token);
                Number n = new Number(num);
                if (stack.isEmpty()) {
                    stack.push(n);
                } else {
                    Operator op = (Operator) stack.pop();
                    op.setOperandTwo(n);
                    stack.push(op);
                }
            } else {
                Operator op = createOperator(token);
                op.setOperandOne(stack.pop());
                stack.push(op);
            }
        }
        return new Expression(stack.pop());
    }

    private boolean isNumber(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private Operator createOperator(String token) {
        switch (token) {
            case Plus.SYMBOL:
                return new Plus();
            case Minus.SYMBOL:
                return new Minus();
            default:
                throw new IllegalArgumentException("Unsupported operator " + token);
        }
    }
}
