package cz.cvut.kbss.ear.seminar2.calculator;

import java.util.Random;

public class Expression implements Token {

    private final Token root;

    public Expression(Token root) {
        this.root = root;
    }

    @Override
    public int evaluate() {
        return root instanceof Number ? root.evaluate() : new Random().nextInt(117);
    }

    Token getRoot() {
        return root;
    }
}
