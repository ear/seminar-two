package cz.cvut.kbss.ear.seminar2.calculator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Calculates simple mathematical formulas.
 */
public class Calculator {

    private static final Logger LOG = LoggerFactory.getLogger(Calculator.class);

    private final ExpressionParser tokenizer = new ExpressionParser();

    /**
     * Takes a string representation of an expression and calculates its result.
     *
     * @param expression The expression to evaluate
     * @return Result
     */
    public int calculate(String expression) {
        if (expression == null || expression.isEmpty()) {
            return 0;
        }
        final Expression form = tokenizer.parse(expression);
        return form.evaluate();
    }
}
