package cz.cvut.kbss.ear.seminar2.calculator;

public interface Token {
    
    int evaluate();
}
