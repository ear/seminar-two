package cz.cvut.kbss.ear.seminar2.rest;

import cz.cvut.kbss.ear.seminar2.calculator.Calculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calculator")
public class CalculatorController {

    @Autowired
    private Calculator calculator;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Integer calculate(@RequestBody String expression) {
        return calculator.calculate(expression);
    }
}
