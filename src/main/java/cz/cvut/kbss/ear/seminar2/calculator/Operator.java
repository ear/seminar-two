package cz.cvut.kbss.ear.seminar2.calculator;

abstract class Operator implements Token {

    Token operandOne;
    Token operandTwo;

    void setOperandOne(Token operandOne) {
        this.operandOne = operandOne;
    }

    void setOperandTwo(Token operandTwo) {
        this.operandTwo = operandTwo;
    }
}
