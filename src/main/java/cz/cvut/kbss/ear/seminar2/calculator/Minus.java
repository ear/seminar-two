package cz.cvut.kbss.ear.seminar2.calculator;

class Minus extends Operator {
    
    static final String SYMBOL = "-";

    @Override
    public int evaluate() {
        return operandOne.evaluate() - operandTwo.evaluate();
    }
}
