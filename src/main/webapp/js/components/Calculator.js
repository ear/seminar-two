'use strict';

import React from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, Panel} from 'react-bootstrap';

import Actions from '../actions/Actions';
import CalculatorStore from '../stores/CalculatorStore';
import Mask from './Mask';

export default class Calculator extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            expression: '',
            loading: false,
            result: null
        }
    }

    componentDidMount() {
        this.unsubscribe = CalculatorStore.listen(this.onFinish);
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    onKeyPress = (e) => {
        if (e.charCode === 13) {
            e.preventDefault();
            this.onCalculate();
        }
    };

    onChange = (e) => {
        this.setState({expression: e.target.value});
    };

    onCalculate = () => {
        Actions.calculate(this.state.expression);
        this.setState({loading: true});
    };

    onFinish = (result) => {
        this.setState({loading: false, result: result});
    };

    render() {
        var mask = this.state.loading ? <Mask text="Calculating..."/> : null;

        return <Panel>
            {mask}
            <Form horizontal>
                <FormGroup>
                    <Col sm={1}>
                        <ControlLabel>Expression</ControlLabel>
                    </Col>
                    <Col sm={4}>
                        <FormControl type="text" value={this.state.expression} onChange={this.onChange} onKeyPress={this.onKeyPress}/>
                    </Col>
                    <Col sm={1}>
                        <Button bsStyle="primary" bsSize="small" onClick={this.onCalculate}>=</Button>
                    </Col>
                    <Col sm={2}>
                        <ControlLabel>
                            {this.state.result}
                        </ControlLabel>
                    </Col>
                </FormGroup>
            </Form>
        </Panel>;
    }
}
