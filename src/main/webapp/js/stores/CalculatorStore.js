'use strict';

import Reflux from 'reflux';
import request from 'superagent';

import Actions from '../actions/Actions';

const URL = 'rest/calculator';

const CalculatorStore = Reflux.createStore({
    listenables: [Actions],

    onCalculate: function (expression) {
        request.post(URL).send(expression, 'text').type('text/plain').end((err, resp) => {
            if (err) {
                    console.log('Error when evaluating expression: ' + expression + '. Status: ' + err.status);
            } else {
                this.trigger(resp.body);
            }
        });
    }
});

export default CalculatorStore;
