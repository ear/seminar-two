'use strict';

import Reflux from 'reflux';

const Actions = Reflux.createActions([
    'calculate'
]);

export default Actions;
