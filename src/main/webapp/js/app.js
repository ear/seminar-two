'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import {PageHeader} from 'react-bootstrap';

import Calculator from './components/Calculator';

const App = () => {
    return <div>
        <PageHeader>Calculator</PageHeader>
        <Calculator/>
    </div>;
};

ReactDOM.render(<App/>, document.getElementById("content"));
